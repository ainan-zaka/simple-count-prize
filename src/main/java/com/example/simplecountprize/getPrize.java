package com.example.simplecountprize;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class getPrize {
    void bulan4(String str) {
        System.out.println("Bulan ke 4");
        int count = 1;

        for (char i : str.toCharArray()) {
            for (char j : str.toCharArray()) {
                for (char k : str.toCharArray()) {
                    for (char l : str.toCharArray()) {
                        String aaa = String.valueOf(i + "" + j + "" + k + "" + l);
                        count = selectionPrize(count, aaa);

                    }
                }
            }
        }
        System.out.println();
    }

    void bulan5(String str) {
        System.out.println("Bulan ke 5");
        int count = 1;

        for (char i : str.toCharArray()) {
            for (char j : str.toCharArray()) {
                for (char k : str.toCharArray()) {
                    for (char l : str.toCharArray()) {
                        for (char m : str.toCharArray()) {
                            String aaa = String.valueOf(i + "" + j + "" + k + "" + l + "" + m);
                            count = selectionPrize(count, aaa);
                        }
                    }
                }
            }
        }
        System.out.println();
    }

    void bulan6(String str) {
        System.out.println("Bulan ke 6");
        int count = 1;

        for (char i : str.toCharArray()) {
            for (char j : str.toCharArray()) {
                for (char k : str.toCharArray()) {
                    for (char l : str.toCharArray()) {
                        for (char m : str.toCharArray()) {
                            for (char n : str.toCharArray()) {
                                String aaa = String.valueOf(i + "" + j + "" + k + "" + l + "" + m + "" + n);
                                count = selectionPrize(count, aaa);
                            }
                        }
                    }
                }
            }
        }
        System.out.println();
    }

    private int selectionPrize(int count, String aaa) {
        // Seleksi Pembayaran yg 3 bulan Telat
        if (!aaa.contains("LLL")) {
            // Seleksi Pembayaran Outstanding yg lebih dari 1
            if (aaa.contains("A")) {
                Pattern p = Pattern.compile("[A]");
                Matcher m = p.matcher(aaa);   // get a matcher object
                int jml = 0;

                while (m.find()) {
                    jml++;
                }

                if (jml < 2) {
                    System.out.println(count++ + ". " + aaa);
                }
            } else {
                System.out.println(count++ + ". " + aaa);
            }
        }
        return count;
    }
}
