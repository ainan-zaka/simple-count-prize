package com.example.simplecountprize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleCountPrizeApplication implements CommandLineRunner {

    private getPrize getPrize;

    @Autowired
    public SimpleCountPrizeApplication(getPrize getPrize) {
        this.getPrize = getPrize;
    }

    public static void main(String[] args) {
        SpringApplication.run(SimpleCountPrizeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        getPrize.bulan4("AO");
        getPrize.bulan5("AO");
        getPrize.bulan6("AO");
    }
}
